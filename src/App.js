import React,  {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Link} from 'react-router-dom';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
import About from './components/pages/About';
import uuid from 'uuid';

class App extends Component{
    state = {
        todoList: [
            {
                id: uuid.v4(),
                title: 'Learn ReactJS',
                complete: false
            },
            {
                id: uuid.v4(),
                title: 'Complete the ReactJS Videos',
                complete: false
            },
            {
                id: uuid.v4(),
                title: 'Installed all necessary tools to run ReactJS',
                complete: false
            }
        ]
    }

    markAsComplete = (id) => {
      this.setState({
        todoList: this.state.todoList.map(todo => {
          if(todo.id === id){
            todo.complete = !todo.complete
          }
          return todo;
        })
      })
    }

    deleteTodoItem = (id) => {
      this.setState({
        todoList: [...this.state.todoList.filter(todo => todo.id !== id)]
      });
    }

    addTodo = (title) => {
      const newTodo = {
        id: uuid.v4(),
        title: title,
        complete: false
      }
      this.setState({
        todoList: [...this.state.todoList, newTodo]
      });
    }

    render(){
        return (
            <Router>
              <div className="App">
                <div className="container">
                  <header style={{background: '#333', color: '#fff', textAlign: 'center', padding: '10px'}}>
                    <h1>To do List</h1>
                    <Link style={{color: '#fff', textDecoration: 'none'}} to = "/" >Home</Link> | <Link style={{color: '#fff', textDecoration: 'none'}} to = "/About">About</Link>
                  </header>
                  <Route exact path="/" render={props =>(
                    <React.Fragment>
                      
                      <AddTodo addTodo={this.addTodo} />
                      <Todos todoObj = {this.state.todoList} markAsComplete = {this.markAsComplete} deleteTodoItem = {this.deleteTodoItem}/>
                    </React.Fragment>
                  )} />
                  <Route path="/About" component={About} />
                </div>
              </div>
            </Router>
        );
    }
}

export default App;
