import React, {Component} from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';

class Todos extends Component{

    render() {
        console.log(this.props.todoObj);
        return this.props.todoObj.map((loopTodoValue) => (
            <TodoItem key = {loopTodoValue.id} loopTodoValue = {loopTodoValue} markAsComplete = {this.props.markAsComplete} 
            deleteTodoItem = {this.props.deleteTodoItem}/>
        ));
    }
}

Todos.propTypes = {
    todoObj: PropTypes.array.isRequired
}

export default Todos;
