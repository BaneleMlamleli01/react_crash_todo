import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class TodoItem extends Component {

    getStyle = () =>{
        return {
            background: '#f4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.loopTodoValue.complete ? 'line-through' : 'none'
        }
    }

    render() {
        const {id, title} = this.props.loopTodoValue;
        return (
            <div style={this.getStyle()}>
                <input type = "checkbox" onChange = {this.props.markAsComplete.bind(this, id)}/> {''}{title}
                <button onClick = {this.props.deleteTodoItem.bind(this, id)} style={btnStyle}>X</button>
            </div>
        )
    }
}

TodoItem.propTypes = {
    loopTodoValue: PropTypes.object.isRequired
}

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    padding: '5px 10px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right'
}

export default TodoItem
